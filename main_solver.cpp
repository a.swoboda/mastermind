#include "handle_cin.h"
#include "game.h"

#include <array>
#include <iostream>
#include <sstream>
#include <tuple>

int main(int const argc, char const* const argv[]) {
    auto gamePegs = Game::Peg{8};
    if (argc > 1 && (std::istringstream{argv[1]} >> gamePegs).fail()) {
        std::cerr << "Failed to read number of colors, defaulting to " << gamePegs << '\n';
    }
    auto gameSize = Game::Size{4};
    if (argc > 2 && (std::istringstream{argv[2]} >> gameSize).fail()) {
        std::cerr << "Failed to read game size, defaulting to " << gameSize << '\n';
    }

    auto const game = Game(gamePegs, gameSize);
    auto const runs = std::size_t{100};
    auto g = game.approximateFirst(runs);
    std::cerr << "My first guess is:\n";
    game.print(std::cout, g);
    std::cout << std::endl;
    std::cerr << "Please enter the result (hits and matches):\n";
    auto [hits, matches] = extract<Game::Size, Game::Size>(std::cin);
    while (!std::cin.good()) {
        std::cerr << "Failed to read, please try again:\n";
        std::cin.clear();
        flushLine(std::cin);
        std::tie(hits, matches) = extract<Game::Size, Game::Size>(std::cin);
    }
    flushLine(std::cin);
    auto possibilities = game.firstMove(g, game.getComparison(hits, matches));
    while (possibilities.size() > 1) {
        std::cerr << "I have narrowed it down to " 
                << possibilities.size() << " possibilities.\n";
        g = game.approximate(possibilities, runs);
        std::cerr << "My next guess is:\n";
        game.print(std::cout, g);
        std::cout << std::endl;
        std::cerr << "Please enter the result (hits and matches):\n";
        std::tie(hits, matches) = extract<Game::Size, Game::Size>(std::cin);
        while (!std::cin.good()) {
            std::cerr << "Failed to read, please try again:\n";
            std::cin.clear();
            flushLine(std::cin);
            std::tie(hits, matches) = extract<Game::Size, Game::Size>(std::cin);
        }
        if (hits == game.size()) {
            return 0;
        }
        flushLine(std::cin);
        game.constrain(possibilities, g, game.getComparison(hits, matches));
    }
    if (possibilities.size() == 0) {
        std::cerr << "All possibilities exhausted: Something went wrong.\n";
        return 1;
    }
    std::cerr << "The solution is:\n";
    game.print(std::cout, *possibilities.begin());
    std::cout << "\n";
}
