#pragma once

#include <type_traits>

namespace Detail {

template <typename I, typename E>
constexpr std::enable_if_t<std::is_integral_v<I> && std::is_unsigned_v<E>, I> ipow_accumulate(I r, I i, E e) {
    while (true) {
        if (e % E{2} == E{1}) {
            r *= i;
            if (e == E{1}) { return r; } 
        }
        i *= i;
        e /= E{2};
    }
}
    
}

template <typename I, typename E>
constexpr std::enable_if_t<std::is_integral_v<I> && std::is_integral_v<E> && std::is_unsigned_v<E>, I> ipow(I i, E e) {
    while (e % E{2} == E{}) {
        i *= i;
        e /= E{2};
    }
    e /= E{2};
    if (e == E{}) { return i; }
    return Detail::ipow_accumulate(i, i*i, e);
}
