#pragma once

#include <type_traits>

template <typename I>
constexpr std::enable_if_t<std::is_unsigned_v<I>, I> gcd(I fst, I snd) noexcept {
    if (fst == I{}) { return snd; }
    if (snd == I{}) { return fst; }
    auto result = I{1};
    while (true) {
        if (fst == snd) { return result * fst; }
        if (fst % I{2} == I{}) {
            fst /= I{2};
            if (snd % I{2} == I{}) {
                snd /= I{2};
                result *= I{2};
            }
        }
        else if (snd % I{2} == I{}) {
            snd /= I{2};
        }
        else if (fst < snd) {
            snd = (snd - fst) / I{2};
        }
        else {
            fst = (fst - snd) / I{2};
        }
    }
}
