# Readme

This project features a CLI Mastermind game (where you have to find the secret code) and a reasonably fast randomized solver.

## Usage

Both executables feature simple command line interfaces (CLI) that work quite similarly.

### Oracle

When using the "oracle" program, you are the codebreaker and "you play against the computer". 

The first argument is the number of colors the code may contain. 

If only one more argument is provided, it is interpreted as the length of the code, and a random code is generated.
If more than one additional argument is provided, the additional arguments are interpreted as the secret and the size inferred therefrom.

The program then asks you to make guesses and in turn provides you with two results: The number of hits and the number of matches (in that order).
The input logic is very simple, write your guess (coding colors as numbers from 0 to the number of N-1 where N is the number of colors) separated by spaces and confirm with "enter".

### Solver

When using the "solver" program, you will hold the secret and "the computer plays against you". 

As above, the executable takes two arguments: The number of colors, and the length of the code (in that order).
If no arguments are provided, the game defaults to length 4 and 8 colors (the game version of my youth).

The program will provide you with guesses (colors coded as integers from 0 to N-1, where N is the number of colors).
In turn, you have to provide the number of hits and matches with your secret (in that order); just separate them with a space and confirm with "enter".

## Building

This project is build with CMake. On Linux, this means:

0. (Optional) Create target directory and cd to it: `mkdir -p path/to/target && pushd path/to/target`
1. Once in the target directory, run CMake: `cmake path/to/project`
2. Build all executables: `make`

That's it!

## Examples
To play a game with length of 4 and 8 colors against the computer:

    ./oracle 8 4

To have the computer play against you in a game with length 4 and 6 colors:

    ./solver 6 4

You can even let both programs play against one another and watch:

    mkfifo pipe && (./solver 8 4)< pipe | tee >(./oracle 8 4) 1>&2 | tee 1>&2 pipe
