#pragma once

#include <iostream>
#include <limits>
#include <tuple>
#include <utility>

void flushLine(std::istream& is) noexcept {
    is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

namespace Detail 
{
    template <typename...Ts, std::size_t...Is>
    auto extract(std::istream& is, std::index_sequence<Is...>) noexcept(noexcept(std::tuple<Ts...>{})) {
        auto result = std::tuple<Ts...>{};
        (is >> ... >> std::get<Is>(result));
        return result;
    }
}

template <typename...Ts>
auto extract(std::istream& is) noexcept(noexcept(Detail::extract<Ts...>(std::declval<std::istream&>(), std::make_index_sequence<sizeof...(Ts)>{}))) {
    return Detail::extract<Ts...>(is, std::make_index_sequence<sizeof...(Ts)>{});
}
