#pragma once

#include "ipow.h"
#include "gcd.h"

#include <algorithm>
#include <array>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <ostream>
#include <random>
#include <sstream>
#include <tuple>

#ifndef MATCH_LOOKUP_TYPE
#define MATCH_LOOKUP_TYPE std::uint_fast64_t
#else
static_assert(std::is_unsigned_v<MATCH_LOOKUP_TYPE>);
#endif

#ifndef COLOR_TYPE
#define COLOR_TYPE std::uint_fast32_t
#else
static_assert(std::is_unsigned_v<COLOR_TYPE>);
#endif

#ifndef SIZE_TYPE
#define SIZE_TYPE std::uint_fast32_t
#else
static_assert(std::is_unsigned_v<SIZE_TYPE>);
#endif

struct Game {
    using Peg = COLOR_TYPE;
    using Size = SIZE_TYPE;

    struct Comparison {
        constexpr auto operator==(Comparison rhs) const noexcept {
            return rep == rhs.rep;
        }
        constexpr auto operator!=(Comparison rhs) const noexcept {
            return rep != rhs.rep;
        }
    private:
        friend class Game;
        using Rep = Size;
        constexpr Comparison(Rep rep) noexcept : rep(rep) {}
        Rep rep;
    };

    constexpr Comparison getComparison(Size hits, Size matches) const noexcept {
        auto const x = length - hits;
        return Comparison{x * (x + Size{1}) / Size{2} + matches};
    }

    constexpr auto hitsAndMatches(Comparison c) const noexcept {
        // not exactly great, but this part is not critical
        auto x = Size{1};
        auto matches = c.rep - Size{1};
        while (matches > x) { 
            ++x; 
            matches = c.rep - (x * (x + Size{1}))/Size{2};
        }
        return std::make_tuple(length - x, matches);
    }

public:
    struct Guess {
        constexpr auto operator==(Guess rhs) const noexcept {
            return rep == rhs.rep;
        }
        constexpr auto operator!=(Guess rhs) const noexcept {
            return rep != rhs.rep;
        }
    private:
        friend class Game;
        using Rep = Size;
        constexpr Guess(Rep rep) noexcept : rep(rep) {}
        Rep rep;
    };

    void print(std::ostream& stream, Guess g) const noexcept {
        for (auto i = Size{}; i != length; ++i, g.rep /= numColors) {
            stream << g.rep % numColors << ' ';
        }
    }

    template <typename It>
    auto asGuess(It from) const noexcept {
        auto result = Guess{{*from}};
        for (auto [multiplier, i] = std::make_tuple(static_cast<Guess::Rep>(numColors), Size{1}); i != length; ++i, multiplier *= numColors) {
            result.rep += *++from * multiplier;
        }
        return result;
    }

    constexpr Comparison compare(Guess lhs, Guess rhs) const noexcept {
        auto hits = Size{};
        // get hits and build the "lookups" for computing the matches
        auto temp0 = lhs.rep;
        auto temp1 = rhs.rep;
        auto lookup0 = Lookup{1};
        auto lookup1 = Lookup{1};
        for (auto i=Size{}; i!=length; ++i, temp0/=numColors, temp1/=numColors) {
            if (temp0 % numColors == temp1 % numColors) {
                ++hits;
            }
            else {
                lookup0 *= primes[temp0 % numColors];
                lookup1 *= primes[temp1 % numColors];
            }
        }
        // compute matches
        auto matches = Size{};
        auto intersection = gcd(lookup0, lookup1);
        for (auto i=Peg{}; i != numColors; ++i) {
            auto const prime = primes[i];
            while (intersection % prime == 0) {
                ++matches;
                intersection /= prime;
            }
        }
        return getComparison(hits, matches);
    }

    struct Possibilities {
        auto size() const noexcept { return possibilities.size(); }
        bool contains(Guess g) const noexcept {
            return std::binary_search(possibilities.begin(), possibilities.end(), g, [](auto lhs, auto rhs) { return lhs.rep < rhs.rep; });
        }
        auto begin() const noexcept { return possibilities.begin(); }
        auto end() const noexcept { return possibilities.end(); }
    private:
        friend class Game;
        using Container = std::vector<Guess>;
        Possibilities(Container container) : possibilities(std::move(container)) {}
        Container possibilities;  // shall be sorted
    };

    auto firstMove(Guess guess, Comparison comp) const noexcept {
        auto possibilities = Possibilities::Container{};
        // NB this can certainly be optimized A LOT
        for (auto i=Guess{{}}, n=Guess{ipow<Guess::Rep>(numColors, length)}; 
                i != n; 
                ++i.rep) {
            if (compare(guess, i) == comp) {
                possibilities.emplace_back(i);
            }
        }
        return Possibilities{std::move(possibilities)};
    }

    void constrain(Possibilities& s, 
            Guess g, 
            Comparison c) const noexcept {
        auto const pastEnd = std::remove_if(begin(s.possibilities), 
                end(s.possibilities), 
                [this, g, c](auto h) { return compare(g, h) != c; });
        s.possibilities.erase(pastEnd, end(s.possibilities));
    }

    Guess randomGuess() const noexcept {
        return Guess{std::uniform_int_distribution{Guess::Rep{}, ipow<Guess::Rep>(numColors, length)-Guess::Rep{1}}(rng)};
    }

private:
    using Evaluation = std::vector<std::uint_fast16_t>;

    constexpr auto numComparisonValues() const noexcept {
        // actually, it is 1 less because we can't have length-1 hits and 1 match
        // but for simplicity, we ignore this
        return ((length + Size{1}) * (length + Size{2})) / Size{2};
    }

    auto evaluate(Guess g, Possibilities const& s) const noexcept {
        auto result = Evaluation(numComparisonValues());
        for (auto p : s.possibilities) {
            ++result[compare(g, p).rep];
        }
        return result;
    }

    auto estimate(Guess g, Possibilities const& s, std::size_t nEvals) const noexcept {
        if (2*nEvals > s.size()) { return evaluate(g, s); }
        auto samples = Possibilities::Container{};
        samples.reserve(nEvals);
        std::sample(begin(s.possibilities), end(s.possibilities), std::back_inserter(samples), nEvals, rng);
        auto result = Evaluation(numComparisonValues());
        for (auto s : samples) {
            ++result[compare(g, s).rep];
        }
        return result;
    }

    auto estimate(Guess g, Possibilities const& s) const noexcept {
        auto evaluation = estimate(g, s, numComparisonValues()*std::size_t{10});
        return *std::max_element(begin(evaluation), end(evaluation));
    }

    auto estimateFirst(Guess g, std::size_t nEvals) const noexcept {
        // NB this can probably be improved A LOT
        auto result = Evaluation(numComparisonValues());
        for (auto i=std::size_t{}; i!=nEvals; ++i) {
            auto const sample = randomGuess();
            ++result[compare(g, sample).rep];
        }
        return result;
    }

    auto estimateFirst(Guess g) const noexcept {
        auto evaluation = estimateFirst(g, numComparisonValues() * std::size_t{20});
        return *std::max_element(begin(evaluation), end(evaluation));
    }

public:
    auto approximateFirst(std::size_t runs) const noexcept {
        auto bestGuess = randomGuess();
        auto bestMax = estimateFirst(bestGuess);
        for (auto i=std::size_t{}; i!=runs; ++i) {
            auto guess = randomGuess();
            auto guessMax = estimateFirst(guess);
            if (guessMax < bestMax) {
                bestGuess = guess;
                bestMax = guessMax;
            }
        }
        return bestGuess;
    }

    auto approximate(Possibilities const& s, std::size_t runs) const noexcept {
        // randomly create the first guess
        auto bestGuess = randomGuess();
        // evaluate the first guess
        auto bestMax = estimate(bestGuess, s);
        // loop, creating and evaluating more guesses, comparing them with the best so far
        for (auto i=std::size_t{}; i != runs; ++i) {
            auto guess = randomGuess();
            auto guessMax = estimate(guess, s);
            if (guessMax < bestMax || (guessMax == bestMax && !s.contains(bestGuess) && s.contains(guess))) {
                bestGuess = guess;
                bestMax = guessMax;
            }
        }
        return bestGuess;
    }

    Game(Peg numColors, Size length) : numColors(numColors), length(length) {
        if (numColors > primes.size()) {
            throw std::range_error("Missing primes in the lookup table.");
        }
        if (std::pow(primes[numColors-1], length)-0.5 > std::numeric_limits<Lookup>::max()) {
            throw std::range_error("Match lookup table is too small. "
                    "Consider recompiling with "
                    "'-DMATCH_LOOKUP_TYPE=std::uint_fast128_t.");
        }
        if (std::pow(numColors, length)-0.5 > std::numeric_limits<Guess::Rep>::max()) {
            throw std::range_error("Guesses won't fit into representation type.");
        }
    }

    constexpr auto size() const noexcept { return length; }
private:
    Peg const numColors;
    Size const length;
    using RNG = std::mt19937;
    static thread_local RNG rng;
    static constexpr auto primes = std::array{Size{2}, Size{3}, Size{5}, Size{7}, Size{11}, Size{13}, Size{17}, Size{19}, Size{23}, Size{27}, Size{29}, Size{31}, Size{37}};
    using Lookup = MATCH_LOOKUP_TYPE;
};
