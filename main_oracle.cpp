#include "handle_cin.h"
#include "game.h"

#include <algorithm>
#include <iterator>
#include <sstream>
#include <vector>

int main(int const argc, char const* const argv[]) {
    if (argc < 3) { 
        std::cerr << "Usage: oracle numColors [codeLength | secret[0] secret[1] ...]\n";
        return -1; 
    }
    auto gamePegs = Game::Peg{8};
    if (argc > 2 && (std::istringstream{argv[1]} >> gamePegs).fail()) {
        std::cerr << "Failed to read number of colors, defaulting to " 
                << gamePegs << '\n';
    }

    auto gameSize = argc - 2;
    if (1 == gameSize) {
        if ((std::istringstream{argv[2]} >> gameSize).fail()) {
            std::cerr << "Failed to read game size\n";
            return -2;
        }
        if (gameSize < 1) {
            std::cerr << "Code length can't be smaller than 1\n";
            return -3;
        }
    }
    auto const game = Game(gamePegs, gameSize);

    auto secret = game.randomGuess();
    
    if (argc > 3) {
        auto vecSecret = std::vector<Game::Peg>{};
        vecSecret.reserve(game.size());
        try {
            std::transform(argv+2, 
                    argv+argc, 
                    std::back_inserter(vecSecret), 
                    [](auto&& cstr) { 
                        auto iss = std::istringstream{cstr}; 
                        auto peg = Game::Peg{};
                        iss >> peg;
                        if (iss.fail()) { throw std::range_error{cstr}; }
                        return peg;
                    });
        }
        catch (std::range_error& e) {
            std::cerr << "Could not read peg " << e.what() << '\n';
            return -4;
        }
        secret = game.asGuess(begin(vecSecret));
    }
    
    std::cerr << "Please enter your first guess, separated by spaces:\n";
    // std::cin.clear();
    auto guess = game.asGuess(std::istream_iterator<Game::Peg>{std::cin});
    while (!std::cin.good()) {
        std::cerr << "failed to read guess, please try again:\n";
        std::cin.clear();
        flushLine(std::cin);
        guess = game.asGuess(std::istream_iterator<Game::Peg>{std::cin});
    }
    // flushLine(std::cin);
    auto [hits, matches] = game.hitsAndMatches(game.compare(secret, guess));
    while (game.size() != hits) {
        std::cout << hits << ' ' << matches << std::endl;
        std::cerr << "Please enter your next guess:\n";
        guess = game.asGuess(std::istream_iterator<Game::Peg>{std::cin});
        while (!std::cin.good()) {
            std::cerr << "failed to read guess, please try again:\n";
            std::cin.clear();
            flushLine(std::cin);
            guess = game.asGuess(std::istream_iterator<Game::Peg>{std::cin});
        }
        std::tie(hits, matches) = game.hitsAndMatches(game.compare(secret, guess));
    }
    std::cerr << "Congratulations! That was correct!\n";
}
